    let map = https://ipfs.io/ipfs/Qmbh2ifwcpX9a384vJMehySbV7rdvYhzVbL5ySs84k8BgY/Prelude/List/map
in  let concat = https://ipfs.io/ipfs/Qmbh2ifwcpX9a384vJMehySbV7rdvYhzVbL5ySs84k8BgY/Prelude/Text/concat
in  let showInteger = https://ipfs.io/ipfs/Qmbh2ifwcpX9a384vJMehySbV7rdvYhzVbL5ySs84k8BgY/Prelude/Integer/show
in  let toInteger = https://ipfs.io/ipfs/Qmbh2ifwcpX9a384vJMehySbV7rdvYhzVbL5ySs84k8BgY/Prelude/Natural/toInteger
in  let showNat = \(n : Natural) -> showInteger (toInteger n)
in  let formatTimeYoutube = \(time : ./Time) -> "${showNat time.hours}h${showNat time.minutes}m${showNat time.seconds}s"
in  let genChapter : Text -> ./Chapter -> Text =
          \(youtubeUrl : Text) ->
          \(chapter : ./Chapter) ->
            ''
            <li><a href="${youtubeUrl}&t=${formatTimeYoutube chapter.time}">${chapter.title}</a></li>
            ''
in  let genLecture : ./Lecture -> Text =
          \(lecture : ./Lecture) ->
            ''
            <h2>${lecture.topics}</h2>
            <ul>
              ${concat (map ./Chapter Text (genChapter lecture.url) lecture.chapters)}
            </ul>
            ''
in  \(lectures : List ./Lecture) ->
      ''
      <!doctype html>
      <html>
      <head>
        <meta charset="utf-8" />
        <title>Kategorientheorie-Vorlesung Inhaltsverzeichnis</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha2/katex.min.js" integrity="sha384-OMvkZ24ANLwviZR2lVq8ujbE/bUO8IR1FdBrKLQBI14Gq5Xp/lksIccGkmKL8m+h" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha2/katex.min.css" integrity="sha384-exe4Ak6B0EoJI0ogGxjJ8rn+RN3ftPnEQrGwX59KTCl5ybGzvHGKjhPKk/KC3abb" crossorigin="anonymous" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha2/contrib/auto-render.min.js" integrity="sha384-cXpztMJlr2xFXyDSIfRWYSMVCXZ9HeGXvzyKTYrn03rsMAlOtIQVzjty5ULbaP8L" crossorigin="anonymous"></script>
      </head>
      <body>
        <h1>Vorlesung Kategorientheorie im WS 2017/18</h1>
        <p>Du hast Verbesserungen? Dann sende einen Merge Request für das <a href="https://gitlab.com/MatthiasHu/kategorientheorie-WS1718">GitLab Repository</a>!</p>
        ${concat (map ./Lecture Text (\(lecture : ./Lecture) -> genLecture lecture ++ "\n\n") lectures)}
        <script>
          renderMathInElement(document.body, {
            delimiters: [
              {left: "$", right: "$", display: false}
            ],
            macros: {
              "\\id": "\\mathrm{id}", // identity morphism
              "\\Id": "\\mathrm{Id}", // identity functor
              "\\Hom": "\\mathrm{Hom}", // Homomorphism set
              "\\End": "\\mathrm{End}", // Endomorphism set
              "\\lim": "\\mathrm{lim}", // Limit
              "\\colim": "\\mathrm{lim}", // Colimit
              "\\Bat": "\\mathcal{B}", // example category
              "\\Cat": "\\mathcal{C}", // example category
              "\\Dat": "\\mathcal{D}", // example category
              "\\Vat": "\\mathcal{V}", // example category
              "\\Fais": "\\mathcal{F}", // Example sheaf ("Faisceau")
              "\\Lan": "\\mathrm{Lan}", // Left Kan Extension
              "\\Ran": "\\mathrm{Ran}", // Right Kan Extension
              "\\SetC": "\\underline{\\mathrm{Set}}", // Category of small sets
              "\\CatC": "\\underline{\\mathrm{Cat}}", // Category of small categories
              "\\MatC": "\\underline{\\mathrm{Mat}}", // Category of matrices
              "\\sSetC": "\\underline{\\mathrm{sSet}}", // Category of simplicial sets
              "\\TopC": "\\underline{\\mathrm{Top}}", // Category of topological spaces
              "\\HausC": "\\underline{\\mathrm{Haus}}", // Category of Hausdorff spaces
              "\\CompHausC": "\\underline{\\mathrm{CompHaus}}", // Category of compact Hausdorff spaces
              "\\CompGenWeakHausC": "\\underline{\\mathrm{CGWH}}", // Category of compactly generated weak Hausdorff spaces
              "\\PointedCompGenWeakHausC": "\\CompGenWeakHausC_*", // Category of pointed compactly generated weak Hausdorff spaces
              "\\AbGrpC": "\\underline{\\mathrm{Ab}}", // Category of abelian groups
              "\\GrpC": "\\underline{\\mathrm{Grp}}", // Category of groups
              "\\RingC": "\\underline{\\mathrm{Ring}}", // Category of rings
              "\\CAlgC": "\\underline{\\mathrm{CAlg}}", // Category of commutative algebras over a field
              "\\ModC": "\\underline{\\mathrm{Mod}}", // Category of modules (over a ring)
              "\\VectC": "\\underline{\\mathrm{Vect}}", // Category of vector spaces (over a field)
              "\\QuivC": "\\underline{\\mathrm{Quiv}}", // Category of quivers
              "\\Sh": "\\underline{\\mathrm{Sh}}", // Category of sheaves
              "\\PSh": "\\underline{\\mathrm{PSh}}", // Category of presheaves
              "\\Ab": "\\underline{\\mathrm{Ab}}", // Category of sheaves of abelian groups
              "\\blank": "{-}", // Category of quivers
              "\\MonC": "\\underline{\\mathrm{Mon}}", // Category of monoids (in a monoidal category)
              "\\ZZ": "\\mathbb{Z}", // Z, the integers
              "\\RR": "\\mathbb{R}", // R, the reals
            }
          });
        </script>
      </body>
      </html>
      ''