# Generating the index

Install [dhall](https://github.com/dhall-lang/dhall-lang):

```bash
$ stack install dhall-text
```

Generate the index HTML file:

```bash
$ echo './generate-index ./lectures' | dhall-to-text > ./index.html
````

Generate the video descriptions:

```bash
$ echo './generate-video-descriptions ./index' | dhall-to-text
````
