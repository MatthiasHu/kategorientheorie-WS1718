\newcommand{\sprache}{de}

\documentclass{uebung}

\begin{document}

\maketitle{8}

\textbf{Notation.}
Sind $f : A \to B$, $g: A \to C$ Morphismen
und existiert das Produkt $B \times C$,
so schreiben wir $(f, g) : A \to B \times C$
für den durch die universelle Eigenschaft des Produktes
induzierten Morphismus.
Ist $1$ ein terminales Objekt,
so schreiben wir für jedes Objekt $A$
den eindeutigen Morphismus als ${!} : A \to 1$.

\vspace{1.5em}

\begin{aufgabe}[10 Punkte]{Inverse bei Monoid- und Gruppenobjekten}
Sei $C$ eine Kategorie mit endlichen Produkten
(insbesondere mit terminalem Objekt).
\begin{teilaufgaben}
  \item
  In jedem Monoid $(M, \cdot, 1)$ in $\Set$
  folgt aus $b_1 \cdot a = a \cdot b_2 = 1$ schon $b_1 = b_2$.
  Zeige allgemeiner für jedes Monoid $(M, \mu, \eta)$ in $C$:
  Sind $f, g_1, g_2 : X \to M$ Morphismen in $C$ mit
  $\mu \circ (g_1, f) = \mu \circ (f, g_2) = \eta \circ {!} : X \to M$,
  so gilt schon $g_1 = g_2$.

  \item
  Zur Definition einer Gruppe in $\Set$
  muss man für Inverse nur fordern $a \cdot a^{-1} = 1$
  und $a^{-1} \cdot a = 1$ folgt automatisch.
  Sei nun allgemeiner $(G, \mu, \eta, \zeta)$ eine Gruppe in $C$,
  also ein Monoid, sodass außerdem untenstehendes Diagramm kommutiert.
  Zeige, dass dieses Diagramm dann
  auch mit $(\zeta, \id_G)$ anstelle von $(\id_G, \zeta)$ kommutiert.
  \[\begin{tikzcd}
    G \ar{r}{(\id_G, \zeta)} \ar{d}{{!}} & G \times G \ar{d}{\mu}\\
    1 \ar{r}{\eta} & G
  \end{tikzcd}\]
\end{teilaufgaben}

\emph{Tipp:
Orientiere dich an den Beweisen für $\Set$,
und zwar am besten in einer Form als eine einzige Gleichungskette
wie $a^{-1} \cdot a = \ldots = 1$.
}
\end{aufgabe}

\begin{aufgabe}[Bonus]{Untermonoidobjekte}
Sei $C$ eine Kategorie mit endlichen Produkten
und $(M, \mu, \eta)$ ein Monoid in $C$.
Sei $i : N \hookrightarrow M$ ein Monomorphismus,
sodass $\eta$ und $\mu \circ (i \times i)$ über $i$ faktorisieren:
$\eta = i \circ \eta^N$, $\mu \circ (i \times i) = i \circ \mu^N$
für gewisse $\eta^N : 1 \to N$, $\mu^N : N \times N \to N$.
Zeige, dass dann $(N, \mu^N, \eta^N)$ ein Monoid in $C$ ist.
\end{aufgabe}

\begin{aufgabe}[10 Punkte + Bonus]{Einheitengruppe eines Monoidobjekts}
Sei $(M, \mu, \eta)$ ein Monoid in $C$.
Wir wollen die folgende im Fall $C = \Set$ mögliche Konstruktion
der Einheitengruppe $M^\times$ verallgemeinern:
\[\begin{array}{c}
 M^\times \defeq \{(x, \tilde{x}) \in M \times M
     \mid x \cdot \tilde{x} = \tilde{x} \cdot x = 1\} \\
   (x, \tilde{x}) \cdot (y, \tilde{y}) \defeq
     (x \cdot y, \tilde{y} \cdot \tilde{x}) \\
  (x, \tilde{x})^{-1} \defeq (\tilde{x}, x)
\end{array}\]

\begin{teilaufgaben}
  \item
  Gib die entsprechende Konstruktion von $M^\times$ zusammen mit
  einem Monomorphismus $k : M^\times \hookrightarrow M \times M$ an.
  Welche Art von Limiten muss in $C$ also
  außer endlichen Produkten noch existieren?

  \item
  Zeige, dass auch die Komposition
  von $k$ mit der Projektion auf die erste Komponente,
  $i \defeq \pi_1 \circ k : M^\times \to M$
  ein Monomorphismus ist.

  \item
  Zeige, dass ein Morphismus $f : X \to M$
  genau dann über $i$ faktorisiert
  (also $f = i \circ f^\times$ für ein $f^\times : X \to M^\times$),
  wenn es einen weiteren Morphismus $g : X \to M$ gibt mit
  $\mu \circ (f, g) = \mu \circ (g, f) = \eta \circ {!} : X \to M$.

  \item
  Finde einen Morphismus $\zeta : M^\times \to M^\times$ mit
  $\mu \circ (i \times i) \circ (\zeta, \id_{M^\times}) =
   \mu \circ (i \times i) \circ (\id_{M^\times}, \zeta) =
   \eta \circ {!} : M^\times \to M$.

  \item
  Zeige, dass $M^\times$ ein Untermonoid von $M$ ist,
  dass also $\mu^\times : M^\times \times M^\times \to M^\times$
  und $\eta^\times : 1 \to M^\times$ existieren
  mit $\mu \circ (i \times i) = i \circ \mu^\times$
  und $\eta = i \circ \eta^\times$.

  \item
  Zeige, dass $i : U(M^\times) \to M$ ein universeller Pfeil
  für den Vergissfunktor $U : \Grp_C \to \Mon_C$ ist.
\end{teilaufgaben}

\emph{%
Die korrekte Bearbeitung von drei Teilaufgaben
reicht für die volle Punktzahl aus.
Was darüber hinaus geht, gibt Bonuspunkte.
}
\end{aufgabe}

\begin{aufgabe}[Bonus]{Monoide in der Kategorie der Monoide}
Sei $C$ eine Kategorie mit endlichen Produkten.
\begin{teilaufgaben}
  \item
  Zeige, dass auch $\Mon_C$ alle endlichen Produkte besitzt.

  \item
  Finde eine einfachere Beschreibung der Kategorie $\Mon_{\Mon_C}$.

  \emph{Tipp:
  Eckmann--Hilton-Argument.
  Und eventuell erst mal für $C = \Set$.
  }
\end{teilaufgaben}
\end{aufgabe}

\begin{aufgabe}[10 Punkte]{Kovollständigkeit von $\Set$ und $\Top$}
Zeige, dass in $\Set$ und in $\Top$ alle kleinen Kolimiten existieren,
indem du jeweils konkrete Konstruktionen angibst
und die universelle Eigenschaft nachweist.
\end{aufgabe}

\begin{aufgabe}[10 Punkte]{Vollständigkeit von Funktorkategorien}
Sei $C$ eine vollständige Kategorie und $D$ eine beliebige Kategorie.
Zeige, dass dann auch $C^D$ vollständig ist.
Folgere, dass $\widehat{C}$
für jede Kategorie $C$ vollständig und kovollständig ist.
\end{aufgabe}

\begin{aufgabe}[Bonus]{Geometrische Realisierung als Koende}
Erkläre, wie die Definition der geometrischen Realisierung $|X|$
einer simplizialen Menge $X$ als Koende
mit folgender Definition I.2.2 aus
\enquote{Methods of Homological Algebra} (Gelfand, Manin)
übereinstimmt:
\[ |X| \defeq (\coprod_{n=0}^{\infty} (\Delta^n \times X_n))/{\sim},\]
wobei $(s, x) \sim (t, y)$, wann immer $s = \Delta^f(t)$, $y = X(f)(x)$
für ein $f : [m] \to [n]$ in $\Delta$,
und $|X|$ die Quotiententopologie trage.
\end{aufgabe}

\end{document}
