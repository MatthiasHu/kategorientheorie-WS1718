\newcommand{\sprache}{de}

\documentclass{uebung}

\begin{document}

\maketitle{3}

\begin{aufgabe}{Definition von Ordinalzahl}
Eine Menge $\alpha$ heißt \emph{Ordinalzahl},
wenn sie transitiv ist
($\forall x \in \alpha\colon \forall y \in x\colon y \in \alpha$)
und für die Elementrelation auf $\alpha$ Trichotomie gilt:
$\forall x, y \in \alpha\colon x \in y \vee x = y \vee y \in x$.

\begin{teilaufgaben}
  \item
  Zeige, dass die Elementrelation auf einer Ordinalzahl $\alpha$
  eine (strikte) Ordnungsrelation,
  also transitiv
  ($\forall x, y, z \in \alpha\colon
    x\in y \wedge y \in z \Rightarrow x \in z$)
  und antisymmetrisch
  ($\forall x, y \in \alpha\colon \neg(x \in y \wedge y \in x)$)
  ist.

  \item
  Zeige, dass die Elementrelation auf einer Ordinalzahl $\alpha$
  sogar eine Wohlordnung ist, das heißt,
  dass jede nichtleere Teilmenge von $\alpha$
  ein minimales Element enthält.

  \emph{Hinweis:
  Das liefert also ein Induktionsprinzip:
  Soll eine Eigenschaft $\varphi$
  für alle Elemente einer Ordinalzahl $\alpha$ geizeigt werden,
  so reicht es,
  $\varphi$ für alle $x \in \alpha$
  mit der Eigenschaft $\forall y\in x\colon \varphi(y)$
  zu zeigen.}

  \item
  Zeige, dass eine Menge genau dann eine Ordinalzahl ist,
  wenn sie transitiv ist und auch
  all ihre Elemente transitive Mengen sind.

  \emph{Tipp für die Rückrichtung:
  Wende zweimal das Fundierungsaxiom
  auf gewisse Teilmengen von $\alpha$ an.
  }
\end{teilaufgaben}
Für Ordinalzahlen $\alpha$, $\beta$
schreiben wir statt $\alpha \in \beta$
auch $\alpha < \beta$ oder $\beta > \alpha$.
\end{aufgabe}

\begin{aufgabe}{Ordnung auf der Klasse aller Ordinalzahlen}
\begin{teilaufgaben}
  \item
  Sei $M$ eine Menge von Ordinalzahlen.
  Zeige, dass $\bigcup M$ eine Ordinalzahl ist.
  \item
  Sei $\alpha$ eine Ordinalzahl.
  Zeige, dass $S\alpha$ eine Ordinalzahl ist.
  \item
  Seien $\alpha, \beta$ Ordinalzahlen.
  Zeige $\alpha < \beta \vee \alpha = \beta \vee \alpha > \beta$.
  \item
  Seien $\alpha, \beta$ Ordinalzahlen.
  Zeige $\alpha < \beta \Leftrightarrow \alpha \subsetneq \beta$.
\end{teilaufgaben}
\end{aufgabe}

\begin{aufgabe}{Nachfolger- und Limesordinalzahlen}
Eine Ordinalzahl $\alpha$ heißt \emph{Nachfolgerordinalzahl},
wenn $\alpha = S\beta$ für eine Ordinalzahl $\beta$.
Eine Ordinalzahl $\alpha$ heißt \emph{Limesordinalzahl},
wenn $\alpha = \bigcup \alpha$.
\begin{teilaufgaben}
  \item
  Zeige, dass jede Ordinalzahl
  eine Nachfolger- oder Limesordinalzahl ist.
  \item
  Zeige, dass nicht beides zugleich der Fall sein kann.
\end{teilaufgaben}
\end{aufgabe}

\newpage

\begin{aufgabe}{Die kumulative Hierarchie}
Die kumulative Hierarchie
wurde in der Vorlesung wie folgt definiert:
\begin{align*}
  V_0 &= \emptyset \\
  V_{\alpha+1} &= \P(V_\alpha) \cup V_\alpha \\
  V_\beta &= \bigcup_{\alpha<\beta} V_\alpha
  \quad \text{(für $\beta$ Limesordinalzahl)}
\end{align*}

\emph{Hinweis:
Solche rekursiven Definitionen auf Ordinalzahlen
lassen sich analog zu rekursiven Definitionen auf $\NN$ formalisieren,
wie wir es auch in Aufgabe 1 von Übungsblatt 2 getan haben.
}

\begin{teilaufgaben}
  \item
  Zeige, dass bei $V_{\alpha+1}$
  die Vereinigung mit $V_\alpha$ unnötig ist,
  da $V_\alpha$ ohnehin transitiv ist
  für jede Ordinalzahl $\alpha$.
  \item
  Zeige, dass für Ordinalzahlen $\alpha < \beta$ gilt
  $V_\alpha \subseteq V_\beta$.
  \item
  Zeige, dass für jede Menge $x$
  eine Ordinalzahl $\alpha$ existiert
  mit $x \in V_\alpha$.
  Nimm dabei ohne Beweis an,
  dass jede Menge Element irgendeiner transitiven Menge ist.
\end{teilaufgaben}
\end{aufgabe}

\begin{aufgabe}{Isomorphismen}
Seien $C, D$ Kategorien und $F : C \to D$ ein Funktor.
Zeige:
\begin{teilaufgaben}
  \item
  Ist $f : X \to Y$ ein Morphismus in $C$
  und sind $g_1, g_2 : Y \to X$
  links- bzw. rechtsinvers zu $f$,
  also $g_1 \circ f = \id_X, f \circ g_2 = \id_Y$,
  so gilt $g_1 = g_2$ und $f$ ist ein Isomorphismus.
  \item
  Isomorphie ist eine Äquivalenzrelation auf $C_0$.
  \item
  Ist $f : X \to Y$ ein Isomorphismus in $C$,
  so ist $F(f)$ ein Isomorphismus in $D$.
  \item
  Sind $X, Y \in C_0$ mit $F(X) \cong F(Y)$
  und ist $F$ volltreu,
  so gilt auch $X \cong Y$.
\end{teilaufgaben}
\end{aufgabe}

\begin{aufgabe}{Einbettung in den Doppeldualruam ist natürlich}
Sei $D^2$ der Doppeldualraum-Funktor
\begin{align*}
  D^2 : \Vect_k &\to \Vect_k \\
  V &\mapsto V^{**} \\
  (f : V \to W) &\mapsto
  (\Lambda \mapsto (\mu \mapsto \Lambda(\mu \circ f)))
  \quad (\Lambda \in V^{**}, \mu \in W^*).
\end{align*}
Zeige, dass die kanonischen Einbettungen in den Doppeldualraum
$\eta_V : V \to V^{**}$
die Komponenten einer natürlichen Transformation
$\eta : \Id_{\Vect_k} \To D^2$
bilden.
\end{aufgabe}

\begin{aufgabe}{Natürliche Transformationen und Partialordnungen}
Seien $C$ eine Kategorie,
$P$ eine partiell geordnete Menge (aufgefasst als Kategorie)
und $F, G : C \to P$ Funktoren.
Zeige: Es gibt genau dann eine natürliche Transformation $F \To G$,
wenn gilt $\forall X \in C_0\colon F(X) \leq G(X)$.
\end{aufgabe}

\begin{aufgabe}{Zentrum nehmen ist kein Funktor}
Zeige, dass es keinen Funktor $\Grp \to \Ab$ gibt,
der jede Gruppe auf ihr Zentrum abbildet.
\end{aufgabe}

\end{document}
